#2023
- Vulfnik - Vulfmon
- MOONSHOTS - The South Hill Experiment
- Soul,PRESENT - Q
- Lahai - Sampha
- Bewitched - Laufey
- Structures Without Rooms - Adam Bosarge

#2022
- Mr. Morale & The Big Steppers - Kendrick Lamar
- Blind - Jameszoo
- 19 Masters - Saya Gray
- Quality Over Opinion - Louis Cole
- World Record - TWEEDEES
- Solar Ash OST - Troupe Gammage, Disasterpeace & Joel Corelitz
- Gemini Rights - Steve Lacy

#2021
- Jubilee - Japanese Breakfast
- Talk Memory - BADBADNOTGOOD
- Promises - Floating Points & Pharoah Sanders
- Mood Valiant - Hiatus Kaiyote
- Inside (The Songs) - Bo Burnham
- Juno - Remi Wolf
- Mercurial World - Magdalena Bay

#2020
- Lianne La Havas - Lianne La Havas
- Shore - Fleet Foxes
- It Is What It Is - Thundercat
- Positions - Ariana Grande
- Djesse Vol. 3 - Jacob Collier
- A Western Circular - Wilma Archer
- 106 - Jacob Mann
- Women In Music, Pt. III - HAIM
- Ricky Music - Porches
- SOUL LADY - YUKIKA
- Magic Oneohtrix Point Never - Oneohtrix Point Never

#2019
- IGOR - Tyler, The Creator
- i,i - Bon Iver
- ANIMA - Thom Yorke
- Rouge - Yuna
- Flamagra - Flying Lotus
- GINGER - Brockhampton
- Circuits - Chris Potter
- When I Get Home - Solange
- Melkweg - Jameszoo & Metropole Orkest
- Thank You for Singing to Me - Bobbing
- Oncle Jazz - Men I Trust

#2018
- Superclean, Vol. II - The Marías
- ye - Kanye West
- Time - Louis Cole
- Mimi - Bad Rabbits
- Suspirium - Thom Yorke
- BALLADS1 - Joji
- Wearing Clothes Must Be a New Experience for You. - Hallucinogenius
- Hill Climber - Vulfpeck

#2017
- Crack-Up - Fleet Foxes
- Clean House - Moon Bounce
- Flower Boy - Tyler, The Creator
- DAMN. - Kendrick Lamar
- No Plan - David Bowie
- Onism - Photay
- Drunk - Thundercat
- Harmony of Difference - Kamasi Washington

#2016
- Blackstar - David Bowie
- Blonde - Frank Ocean
- "Awaken, My Love!" - Childish Gambino
- 24K Magic - Bruno Mars
- A Moon Shaped Pool - Radiohead
- IV - BADBADNOTGOOD
- MY WOMAN - Angel Olsen
- 25 - Adele
- Man About Town - Mayer Hawthorne
- Sadie - Photay
- Aziza - Aziza

#2015
- Thrill of the Arts - Vulfpeck
- The Beyond / Where the Giants Roam - Thundercat
- The Phosphorescent Blues - Punch Brothers
- Choose Your Weapon - Hiatus Kaiyote
- To Pimp a Butterfly - Kendrick Lamar

#2014
- Singles - Future Islands
- Black Messiah - D'Angelo
- White Women - Chromeo
- Nabuma Rubberband - Little Dragon
- You're Dead! - Flying Lotus

#2013
- Apocalypse - Thundercat
- Until The Quiet Comes - Flying Lotus
- The 20/20 Experience - Justin Timberlake
- American Love - Bad Rabbits
- Comedown Machine - The Strokes
- Random Access Memories - Daft Punk
- Endless Fantasy - Anamanaguchi
- Wolf - Tyler, The Creator
- Pale Machine - Bo En
- Where Does This Door Go - Mayer Hawthorne
- Doris - Earl Sweatshirt
- Reflektor - Arcade Fire

#2012
- channel ORANGE - Frank Ocean
- Unorthodox Jukebox - Bruno Mars
