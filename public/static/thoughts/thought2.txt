For a long time I was one of those anti-sports people. Sportsball. Throw the dead pig will ya! He ran and did a goal! No thanks, I don't watch sports, it's a waste of time. It's just lame gladiator games for modern times. Etc.

And for the most part, I still agree with much of that sentiment. I am weirded out by people who schedule their entire Sunday—every Sunday—around NFL games. There's an incredible amount of money and attention being driven towards men and women playing games. All of that said, I love basketball.

You can probably learn the same lessons from many sports, but there are two defining characteristics of basketball that highlight these lessons: intimacy and teamwork. Intimacy may be a strange word for it, but there are only ten people on a court at a time; five for each team. Each of the five players on a team are playing an individual role.

#Point Guard

A generally shorter player who brings the ball into play. They analyze who is on the floor and find ways to take the play the coach drew up and make it work against the current defense. These players can handle the ball well and are great passers. Think [Chris Paul](https://www.youtube.com/results?search_query=chris+paul+highlights), [Steve Nash](https://www.youtube.com/results?search_query=steve+nash+highlights), [Steph Curry](https://www.youtube.com/results?search_query=steph+curry+highlights), [Magic Johnson](https://www.youtube.com/results?search_query=magic+johnson+highlights). There are quite a few more players who should make this list, but I'm cutting it at four.

#Shooting Guard

As the title suggests, these players tend to be pretty good at shooting the ball. They often can rack up a ton of points. Often times they are also quite good at defence. Think [Michael Jordan](https://www.youtube.com/results?search_query=michael+jordan+highlights), [Kobe Bryant](https://www.youtube.com/results?search_query=kobe+bryant+highlights), [James Harden](https://www.youtube.com/results?search_query=james+harden+highlights), [Jerry West](https://www.youtube.com/results?search_query=jerry+west+highlights).

#Small Forward

In the modern game, these players are kind of generalists. They can score, defend, assist, and are tall enough to rebound. The best ones have as good ball handling skills as point guards. Think [LeBron James](https://www.youtube.com/results?search_query=lebron+james+highlights), [Scottie Pippin](https://www.youtube.com/results?search_query=scottie+pippin+highlights), [Julius Erving](https://www.youtube.com/results?search_query=julius+erving+highlights), [Paul George](https://www.youtube.com/results?search_query=paul+george+highlights). Again, left a few off, just too many great small forwards.

#Power Forward

A tall scorer and rebounder. These players can back down players in the paint to hit a turn around jumper, catch alley-oop lobs from point guards and are the first line of inner defence against the rim. Think [Tim Duncan](https://www.youtube.com/results?search_query=tim+duncan+highlights), [Dirk Nowitzki](https://www.youtube.com/results?search_query=dirk+nowitzki+highlights), [Kevin Garnett](https://www.youtube.com/results?search_query=kevin+garnett+highlights), [Charles Barkley](https://www.youtube.com/results?search_query=charles+barkley+highlights).

#Center

Usually the tallest teammmate. Traditionally they hang out under the basket as a last line of defence, with the ability to easily block shots from smaller players. They are also great inside scorers, nearly unstoppable when they have the ball near the rim. In more modern times, some centers have adopted the three-point shot and assisting others to round out their game. There are an incredible amount of legendary centers, but I will choose four. Think [Bill Russell](https://www.youtube.com/results?search_query=bill+russell+highlights), [Kareem Abdul-Jabbar](https://www.youtube.com/results?search_query=kareem+abdul-jabbar+highlights), [Shaquille O'Neal](https://www.youtube.com/results?search_query=shaq+highlights), [Nikola Jokić](https://www.youtube.com/results?search_query=nikola+jokic+highlights).

#Artistry in Motion

With the positions broken down, let's examine one of LeBron James's performances that is in the upper echelons of peak athletic command. LeBron has had many moments that people will point to when talking about a player "taking over a game." The one I'd like to look at is [Game II of the 2018 Eastern Conference Semi-Finals](https://www.youtube.com/watch?v=jdXjerxt6vQ). LeBron's Cleveland Cavaliers against the Toronto Raptors.

LeBron has already been in peak form, dismantling the Raptor's defence in Game I. However something special starts to happen in Game II near the end of the third quarter. Cleveland is up 11 points on Toronto, 89 to 78. LeBron has the ball and starts driving on his defender, a young Pascal Siakam. James starts moving forward with immense energy several feet outside of the three-point line. Siakam, knowing that James is quite a bit bigger than him, makes a move to block the attacker around the free throw line, but LeBron moves to his right to keep driving forward. Siakam eventually gets the situation under control, stopping LeBron near the right-side low block and this stop causes LeBron to lose the ball for a second but he gathers, quickly turns around and throws in a fadeaway jumper above Siakam. The ball goes in.

I don't know if you've ever practiced your turnaround fadeaway jumpers, but even for NBA players they are a low percentage shot due to falling away from the basket when shooting. They are also ill-advised as you are moving away from the basket and cannot get the rebound when the ball more than likely does not go in.

Next possession. LeBron takes a few dribbles outside the three-point line—again defended by Siakam—and splashes a rocket-like, almost straight-line three-point shot. Fourth quarter, defended by Kyle Lowry. LeBron is inside the arc, back to the basket, on the right wing. He's taking his time, looking for a good assist, and deciding that one isn't coming up, so he fakes left, spins to the right and hits another fadeaway jumper above Lowry. In. Next possession. LeBron is at the top of the key, backing down Lowry. Lowry isn't letting him past, so LeBron spins to his left this time, and launches another fadeaway jumper. In. All Net. Next possession. Defended by Siakam again, this time on the left wing. He's facing Siakam head on, and Siakam is doing his best to keep his hand in LeBron's face, hopefully defending against another jumper. LeBron instead takes an unexpected dribble to the left and launches another leftward-drifting fadeaway. Splash. The Toronto crowd starts to jeer now. Next possession. LeBron has gotten OG Anunoby switched on to him, drives towards the top of the key and takes another fadeaway. In. The body language of the Toronto players is making itself apparent, they cannot stop this man. A few minutes later. Lebron is backing down DeMar DeRozan on the left side. He spins to his right, throws up another fadeaway and it goes in. Next possession he performs another turnaround fadeaway jumper at the top of the key against Anunoby. The crowd is actively getting upset at every shot he takes. LeBron ended with 43 points that game.

Cleveland eventually won the series 4 games to 0. In the offseason the Raptors traded away their star player and got a new coach. Turns out one man can change the course of an opposing team.

#Lessons

What is it about intimacy and teamwork that makes basketball unique? Let's examine the relationship between the coach and the twelve to fifteen players. First, there is something special about that amount of people sharing the same goal. There is evidence that the [maximum number of direct reports of a manager should be 10](https://hbr.org/2012/04/how-many-direct-reports). You can imagine, only about 10 of those 12-15 players get to see a lot of court-time, so a coach generally fits that requirement.

Most importantly is how leadership works on a basketball team. Michael Jordan was known to be harsh on his teammates, but was he a tyrant who told people what to do, or a leader that pushed his teammates to be on the same level as him? There is a hesitancy in the corporate world to push people at this level, but a lot of times that's because the "leadership" isn't operating at an inspirational level, so they end up being tyrants.

Finally, let's look at the aspect of consistency on a team. When you have a position or role on a basketball team, you are constantly practicing the skills of that role over the course of the season. Centers block shots and dunk. Shooting guards are practicing their three-pointers and defense. You depend on your team go have done their work so you can go fit in and do yours. Many teams in the non-sports world don't know how to operate efficiently like this, and the trust that should be had between teammates erodes.

#In Conclusion

Basketball is my favorite sport.
