# Pollen Books

## Overview

A collection of "books" to be published on the web. We are using [Pollen](https://docs.racket-lang.org/pollen/index.html) to define them.

## Installation

There is an `info.rkt` file that describes dependencies for this project. You can run:

    > raco pkg install

and this will install all the dependencies defined in that file.

## Generating HTML

Running

    > raco pollen render <file>

will at least get you one file. I need to read more to see what will build a whole project.

And to start a project server:

    > raco pollen start

### Publishing

And finally, to publish:

    > raco pollen publish <src-dir> <dest-dir>

So, for example, this could be (assuming you're in the `pollen-publisher` directory):


    > raco pollen publish my-pollen-book ../public/pollen/my-pollen-book
