#lang pollen

◊define-meta[meta-title]{Memories of Timo}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
The travelers meet at a crossroads.

This is my image. I hope it comes out well:
  ◊pimg["static/images/arkdale.png" css-page-width css-page-width]{
A map of the lower west side of the region. Our travelers have met just south of Arkdale, where the roads converge.
  }
And more text below, just because. Want to see if it gets really messed up.
}