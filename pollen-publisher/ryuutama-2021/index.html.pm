#lang pollen
◊(require "./index.ptree")
◊(require pollen/pagetree)

◊define-meta[meta-title]{Table of Contents}

◊(define pc (pagetree->pollen-context select doc children other-siblings))

◊div[#:class "book-cover"]{
  ◊h1[#:class "book-title"]{
  ◊hr{}
  Ryuutama 2021
  ◊hr{}
  }
  ◊toc[pc]{}
}