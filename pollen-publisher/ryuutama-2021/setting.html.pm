#lang pollen

◊define-meta[meta-title]{Setting}
◊(define mc (metas->meta-context select metas))

◊page[mc]{
Three hundred years ago, the moon fell. Not all the way of course, but enough to change the world. Pockets of land have risen from the soil, hanging upwards of 1000m in the sky. Some large, some small. Some barren, some rich in life and civilization, though contact has been lost in the three centuries that have passed.

◊pimg["static/images/floating-island-1.png" css-page-width css-page-width]{
Could someone be living up here?
  }

The one commonality between all floating islands, as denizens have called them, is that they house bodies of water. Whether an untouched underground lake, or part of a river that now cascades down from the heavens, water has determined the fate of the world’s geography. Some floating islands quickly lost their water and fell back to the planet. Sometimes in the right place, sometimes not. Others are slowly descending, and rumors are starting to spread.

What could be on the floating islands? Not a soul has been up there for 300 years. Are the legends of a missing civilization that survived true? Treasures lost to time? Some say they can hear the roars of a ◊ple["http://kotohi.com/ryuutama/the-world-of-ryuutama/"]{dragon} echoing down from above.

Small encampments have started to crop up around a particular floating island, “The Sun Obstructer”. Treasure hunters, spectators and lucrative business owners of all kinds have flocked to the area to take advantage of the excitement around its descent.
}