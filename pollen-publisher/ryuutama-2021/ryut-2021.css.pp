#lang pollen

◊(require "../common-pollen/base.css.pp")

◊define[ryut-colors]{
  ◊raw->css-colors{
    #3d322a
    #fdfffe
    #e6decf
    #458a99
    #84bec2
    #dd996e
    #fcf1be
  }
}

◊doc

body {
  font-family: 'Bellefair', serif;
  color: ◊|(css-colors-black ryut-colors)|;
  background-color: ◊|(css-colors-white ryut-colors)|;
}

h1,
h2,
h3,
h4,
h5 {
  hyphens: none;
}

.page-next > span, .page-prev > span {
  margin-left: 40%;
  color: ◊|(css-colors-primary ryut-colors)|;
}
.page-next:hover, .page-prev:hover {
  background-color: ◊|(css-colors-light-gray ryut-colors)|;
}

.footer {
  font-size: 2em;
}
.footer a, .footer a:visited {
  color: ◊|(css-colors-primary ryut-colors)|;
  text-transform: uppercase;
}
.footer a:hover {
  color: ◊|(css-colors-light-gray ryut-colors)|;
}

.book-title {
  font-family: "Merienda", sans-serif;
  font-size: 3em;
  text-align: center;
  color: ◊|(css-colors-dark ryut-colors)|
}

.table-of-contents a {
  color: ◊|(css-colors-black ryut-colors)|;
}
.table-of-contents a:hover {
  color: ◊|(css-colors-secondary ryut-colors)|;
  background-color: ◊|(css-colors-highlight ryut-colors)|;
}

.chapter-title {
  font-size: 4em;
  text-transform: uppercase;
  max-width: 50%;
}
.chapter-section {
  display: inline;
  font-size: 2.5em;
}
.chapter-section > a {
  max-width: 50%;
}

.page {
}
.page-title {
  color: ◊|(css-colors-dark ryut-colors)|;
  font-size: 6em;
  border-bottom: 5px dotted ◊|(css-colors-dark ryut-colors)|;
  text-align: right;
  ◊(define pt-margin "40px")
  padding-top: ◊|pt-margin|;
  margin-bottom: ◊|pt-margin|;
}
.page p, .page li {
  font-size: 3.5em;
  line-height: 1.5em;
}

.page-link, .page-link:visited {
  color: ◊|(css-colors-black ryut-colors)|;
  font-size: .8em;
}
.page-link:hover {
  color: ◊|(css-colors-secondary ryut-colors)|;
  background-color: ◊|(css-colors-highlight ryut-colors)|;
}

p.page-image-text {
  color: ◊|(css-colors-primary ryut-colors)|;
  font-size: 3em;
  line-height: 1.1em;
}

/* Media Query for non-mobile pages */
◊css-media-width {
  .footer-top {
    border-left: 1px solid ◊|(css-colors-light-gray ryut-colors)|;
    border-right: 1px solid ◊|(css-colors-light-gray ryut-colors)|;
  }

  .footer {
    font-size: 1.2em;
  }

  .chapter-title {
    font-size: 2em;
  }

  .chapter-section {
    font-size: 1.7em;
  }

  .page-title {
    font-size: 4em;
  }

  .page p, .page li {
    font-size: 2em;
    line-height: 1.2;
  }

  p.page-image-text {
    font-size: 1.5em;
    line-height: 1.1em;
  }

}
