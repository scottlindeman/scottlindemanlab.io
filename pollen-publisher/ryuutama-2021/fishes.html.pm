#lang pollen

◊define-meta[meta-title]{Awake with the Fishes}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
A funeral, bedside, fishing and an encounter with wolves.
}