#lang pollen

◊define-meta[meta-title]{The Combat Kings}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
Left at the Englet starport, the Travellers come across an older shop keeper. The Travellers want a ship, and the shopkeeper says he can get them one, for a discount even, if they find his daughter.
}