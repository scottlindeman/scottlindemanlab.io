#lang pollen

◊define-meta[meta-title]{Fake It Till You Make It}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
Convincing the pirates of the team's legitimacy.
}