#lang pollen
◊(require "./index.ptree")
◊(require pollen/pagetree)

◊define-meta[meta-title]{Table of Contents}

◊(define index-pollen-context (pagetree->pollen-context select doc children other-siblings))

◊div[#:class "book-cover"]{
  ◊h1[#:class "book-title"]{
    ◊em{Traveller 2018}
  }
  ◊toc[index-pollen-context]{}
}