#lang pollen

◊define-meta[meta-title]{Setting}
◊(define mc (metas->meta-context select metas))

◊page[mc]{
It is a time of expansion for the ◊ple["https://en.wikipedia.org/wiki/Traveller_(role-playing_game)"]{Third Imperium}. A recent survey of The Eastern Marches has revealed the locations of several previously unknown star systems. Of course, the Imperium cannot hold on to secrets for too long. Leaked reports about the new systems have revealed many of the planets are rich in familiar minerals and jewels.

As the news of these new Unincorporated Star Systems (USTs) and their potential makes its way down through the noble elite and merchants, a large migration has started. Lesser nobles vying for seats of power in new systems, speculators, drifters trying to make a quick buck, and more are quickly making their way to this previously less populated region of the Imperium. Additionally, a fairly prominent anti-colonial group has formed in the region in response to the sudden influx of greed.

In fact, not long after the discovery of the new USTs, the Imperium quickly incorporated two nearby systems. As they become integrated with Imperium rules and regulations, they are considered off-limits for non-official visits. This is a warning sign for speculators that if they are not the first ones to reach a system, then it is unlikely they will play an important role in the new economy or rule of law.

While much of this news is beneficial for the Imperium, there looms a potential conflict with the nearby border of the K’kree inhabited Two Thousand Worlds Hegemony. Known as fearsome, space-faring sophonts, it is unclear how much the Imperium has communicated with the Hegemony when incorporating planets in this sector. Will the K’kree also colonize? Has there been an agreement? If so, what has the Imperium agreed to do in return?
}