#lang pollen

◊(require "../common-pollen/base.css.pp")

◊define[tvlr-colors]{
  ◊raw->css-colors{
    #000000
    #fcfcfc
    #f0f0f0
    #2929cc
    #db143c
    #fff200
    #56b357
  }
}
◊(define big-border-size "10px")
◊(define p-font-size "1.7em")

◊doc
body {
  font-family: "Rokkitt", sans-serif;
  color: ◊|(css-colors-black tvlr-colors)|;
  background-color: ◊|(css-colors-white tvlr-colors)|;
}

h1,
h2,
h3,
h4,
h5 {
  font-family: "Coda", sans-serif;
  hyphens: none;
}

.page-next > span, .page-prev > span {
  color: ◊|(css-colors-black tvlr-colors)|;
}
.page-next {
  border-right: ◊|big-border-size| solid transparent;
}
.page-next:hover {
  background-color: ◊|(css-colors-light-gray tvlr-colors)|;
  border-right: ◊|big-border-size| solid ◊|(css-colors-highlight tvlr-colors)|;
}
.page-prev {
  border-left: ◊|big-border-size| solid transparent;
}
.page-prev:hover {
  background-color: ◊|(css-colors-light-gray tvlr-colors)|;
  border-left: ◊|big-border-size| solid ◊|(css-colors-highlight tvlr-colors)|;
}

.footer {
  font-size: 3em;
}
.footer a, .footer a:visited {
  color: ◊|(css-colors-black tvlr-colors)|;
  text-transform: uppercase;
}
.footer a:hover {
  color: ◊|(css-colors-highlight tvlr-colors)|;
}

.book-title {
  color: ◊|(css-colors-primary tvlr-colors)|;
  font-size: 12em;
  text-align: center;
  writing-mode: vertical-rl;
  -webkit-writing-mode: vertical-rl;
  transform: rotate(180deg);
  text-transform: uppercase;
  letter-spacing: 2px;
  line-height: 134px;
  background-color: ◊|(css-colors-black tvlr-colors)|;
  border-right: ◊|big-border-size| solid ◊|(css-colors-primary tvlr-colors)|;
  margin-left: ◊|(css-map-value - big-border-size)|;
}

.table-of-contents {
  border-top: ◊|(css-map-value (λ (v) (* 2 v)) big-border-size)| solid ◊|(css-colors-primary tvlr-colors)|;
}

.table-of-contents a {
  color: ◊|(css-colors-black tvlr-colors)|;
}
.table-of-contents a:hover {
  background: ◊|(css-colors-secondary tvlr-colors)|;
}

.chapter-title {
  font-family: "Coda", sans-serif;
  font-weight: bold;
  font-size: 4em;
  text-transform: uppercase;
}

.chapter-section {
  display: inline;
  font-size: ◊(css-map-value (λ (v) (* 2 v)) p-font-size);
}

.chapter-section > a {
  max-width: 50%;
}

.page {
  border-top: ◊|(css-map-value (λ (v) (* 2 v)) big-border-size)| solid ◊|(css-colors-primary tvlr-colors)|;
  border-left: ◊|big-border-size| solid transparent;
  border-right: ◊|big-border-size| solid transparent;
}

.page-title {
  color: ◊|(css-colors-black tvlr-colors)|;
  font-size: 5em;
  text-transform: uppercase;
  border-bottom: 5px dotted ◊|(css-colors-black tvlr-colors)|;
  padding-top: 40px;
}

.page.right {
  margin-left: ◊|css-page-runner-width|;
}

.page p, .page li {
  font-size: ◊(css-map-value (λ (v) (* 3 v)) p-font-size);
  line-height: 1.5em;
}

.page-link, .page-link:visited {
  color: ◊|(css-colors-black tvlr-colors)|;
}
.page-link:hover {
  background-color: ◊|(css-colors-secondary tvlr-colors)|;
}

p.page-image-text {
  color: ◊|(css-colors-highlight tvlr-colors)|;
  font-size: 3em;
  line-height: 1.5em;
}

/* Media Query for non-mobile pages */
◊css-media-width {
  .footer-top {
    border-left: 1px solid ◊|(css-colors-black tvlr-colors)|;
    border-right: 1px solid ◊|(css-colors-black tvlr-colors)|;
  }

  .footer {
    font-size: 1.5em;
  }

  .book-title {
    font-size: 10em;
    letter-spacing: 2px;
    line-height: 134px;
  }

  .chapter-title {
    font-size: 2.5em;
  }

  .chapter-section {
    font-size: ◊|p-font-size|;
  }

  .page-title {
    font-size: 3em;
  }

  .page p, .page li {
    font-size: ◊|p-font-size|;
    line-height: 36px;
  }

  p.page-image-text {
    font-size: 1.5em;
    line-height: 1.1em;
  }

}
