#lang pollen

◊define-meta[meta-title]{Let the Games Begin}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
The outcome of the first job, the arrival of rival pirate orgs and the first game. I believe this was the space one.
}