#lang pollen

◊define-meta[meta-title]{Lorenzo "Scratchy" Atreides}
◊define[mc]{◊(metas->meta-context select metas)}
◊define[pc]{◊(pagetree->pollen-context select)}

◊define[lfirst]{Lorenzo}
◊define[lmiddle]{"Scratchy"}
◊define[llast]{Atreides}

◊page[mc]{

◊lfirst ◊lmiddle ◊llast was born to minor nobles in the core of the Imperium. He has always been short, but the weight and sweatiness only started accumulating in the last few years. That's fine by him, the inhabitants of the Imperium recognize him nearly exclusively by his words rather than his mustachioed and sweaty face. Not many alive today can say they've captured the minds of a galaxy through tales.

Childhood wasn't too bad. Mother and Father were both absent, but that's normal for low nobility. Most of their time was spent trying (and failing) to rise in power on their homeworld. ◊llast can't even recall his homeworld; once he left, he didn't go back. Thanks for the memories folks.

However, that doesn't mean ◊llast didn't use his noble status when applying for schools. Not quite sure what he wanted to do, but quite sure he wanted to get off his birth rock, he accepted the first law program that came his way: Jhonto Law School, a three week jump from home. That should do the trick.

Law school was mostly forgettable, outside of the somewhat legendary parties ◊llast threw. Being of nobility does make one a schmoozer. If one was judging ◊lfirst by his grades, he would be considered phenomally mediocre. Yet if one looked only at his briefs, his genius was already present, if raw. After graduating, he knew he and law were not to be wedded. And so, after kicking around for a few months in the Outer Palisades, he took up a job writing local interest stories.

He earned the nickname ◊lmiddle due to the chicken-scratch notes he took when interviewing locals. Soon, his words started to make their way out of the Palisades and printed in media in other systems. He was quick to drop the local gig, there were better opportunities out there.

Over the course of several years, and only going by his pseudonym, ◊lmiddle carved out a niche of long-form gonzo pieces. His highly-personal, objectively-lite stories about the underbelly of the Imperium were an instant success. Many readers thought it was impossible one man could have produced so much exhilarating output.

◊pimg["static/images/scratchy-expose.jpeg" "600px" "900px"]{
Letting the stars out of the jar, so to speak.
}

A selection of famous pieces:
  ◊ol[#:class "sqrnd"]{
   ◊li{
A sentimental depiction of meeting injured veterans at a charity fundraiser for the victim of the war. ◊pli['lectron.html pc]{} was a major source for this story.
    }
    ◊li{
A heavily fictionalized account of time in a psionics institute, where he did not excel. However, ◊lmiddle did meet a young woman named ◊pli['rauler.html pc]{Cannie} who helped him along the way as best she could. This story almost didn't make it to print, due to an outside of the law exchange that left ◊llast needing to put up collateral. Somehow the story was recovered in an escapade that required a ship with the data aboard it be boarded, the data recovered, and the ship destroyed (all souls lost).
    }
    ◊li{
A description of the corruption and decadence of the ducal rulers of one of the sectors bordering the Eastern Marches. The writing of this story precipitated a series of hard questions by a very high ranking member of Imperial intelligence, who was quite pleased to have political cover for eliminating a royal rival. The man was a Lord, from somewhere far away, and he feels as though he owes ◊llast .
    }
    ◊li{
A description of low berth travel, which was a sensation in (wealthy) areas where the practice is either uncommon or illegal. This involved ◊llast taking several low berth trips himself.
    }
    ◊li{
Half of a first ever true blue "investigative" piece, which got him into enough trouble that he lost his job and it was heavily suggested that he should make himself scarce.
    }
  }
Over his travels, ◊llast acquainted himself with the use of heavy weaponry. He's never mentioned killing someone in his stories, but his repeated use of the phrase "I couldn't get in contact with X anymore, so I continued on◊hentity{hellip}" leaves much for interpretation.
}