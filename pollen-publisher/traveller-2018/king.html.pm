#lang pollen

◊define-meta[meta-title]{A Pirate King}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
The last two games. The second one is the race to find something in the deep dark woods. The third one is the hand to hand combat.
}