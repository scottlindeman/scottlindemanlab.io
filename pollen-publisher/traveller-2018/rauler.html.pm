#lang pollen

◊define-meta[meta-title]{Dr. Cannie Rauler}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊define[clong]{Canalysse}
◊define[cfirst]{Cannie}
◊define[clast]{Rauler}

◊define[sfirst]{Sahara}

◊page[mc]{
◊clong ◊clast was born to an average family, of average wealth and ability, on an average planet, during an average time. From her beginnings as a middle child, she was immediately met with an existential worry about just being forgettable, and turned that into a drive to be extraordinary.

When ◊cfirst realized school came more easily to her than some others, she set decided this was her extraordinary calling. Aiming to be a top student, she doubled down on her studies, while keeping her extra-curriculars in order. She managed to succeed, becoming not only the “good student” of her family, but also the star pupil of her school.

It wasn't enough; she needed more. Thus, she applied to the top system universities and was accepted into the prestigious Lornard University, which meant, being from an average planet, she was already greatly surpassing her peers. So she left her homeworld to learn.

◊; "Contributing value" isn't great
Deciding that she could be extraordinary while contributing value, she decided to focus on Medicine in school. She took a number of other courses while in school, spent her time joining clubs, learning about places other students had come from, falling in love with romantic ideals of space travel to distant lands, learning about space travel and the far reaches. However, ever part of her desire to remain at the top, she spent the majority of her time focused on her schoolwork.

◊; Fix this paragraph
At some point during her tenure in school, she happened to meet a young woman who seemed to be living a much more exciting life than she was. As a girl with a taste for the extraordinary, meeting this young adrenaline junkie would have a major impact on her life. ◊sfirst Zendrani spent her days as a hair stylist, but spent her nights zipping around the planet on a ground-bike and partying hard. ◊sfirst enjoyed showing her somewhat naive but bluff friend the ropes, and ◊cfirst was having a blast. ◊cfirst found herself leading a much more fun life for a while, although after the initial taste of excitement it offered, did keep herself pretty firmly grounded in her studies.

◊; They should meet at the psionics institute
It was through ◊sfirst that she ended up at a party where she met a journalist who called himself ◊pli['scratchy.html pc]{"Scratchy"} and happened to learn about a way in which she could prove herself even more extraordinary. Scratchy keyed ◊cfirst into the existence of major psionic institutes within the Imperium, and gave her the hint that getting herself tested could be a way to set her apart. She managed to net herself a business card and a lot of options to mull over.

◊; This basically says nothing about her time there. Need to expand, this should be the most interesting part.
◊cfirst knew that psionics were generally frowned upon as unsavory and ethically questionable, but once the thought had been placed into her head she couldn’t stop thinking about it. She had dabbled with the illegal before, mainly under the influence of her more exciting friend, and decided after much deliberation to spend a summer taking a break from her new career and attending an underground and very illegal psionic institute. It sent her into a serious amount of student loan debt, a new experience for her after her very generous scholarship had gotten her through school thus far, but she tested in and showed a lot of potential, and was unwilling to give up on a chance to prove herself extraordinary in another dimension.

Now saddled with a remarkable amount of debt, and a remarkable new power, young Dr. ◊clast returned to work as a doctor. However, she found herself unwilling to reveal her secret, and consequently unable or unwilling to take real advantage of her new psychic prowess while living so close to her home, her school, and major population centers she could be cast out of.

She reconnected with Scratchy during this period, and managed to discover that he was not only a remarkable partier and author, but a psychic as well. They had a number of deep chats about her concerns and what had changed in her life pre-and-post psionic awakening. She managed to learn a lot about her own powers through these discussions, and Scratchy went on to write a pretty prolific piece that ◊cfirst liked to think she had played a major part in.

◊sfirst suggested, on a whim, that they should move to the rim. They weren’t too distant from it already, and getting board on a ship heading that way couldn’t be too hard for a talented doctor and her talented hairdresser. This offer solved a lot of problems for Canny, giving her a way to possibly really help with her medical skills, along with a real chance to strike it big on the frontier, and a chance to practice her psionics with much less fear of being ostracized.

So they moved to the frontier. After arriving on the frontier, ◊cfirst and ◊sfirst ending up drifting in different directions, with ◊cfirst more bound to places she could practice her medicine, while ◊sfirst found herself drifting much more freely. They remain close, but haven’t spoken in a while.

During her early days on the frontier, Dr. ◊clast helped fix up an Aslan named ◊pli['caito.html pc]{} after a moderate severity G/Bike accident. Having developed a reasonable rapport with him during his short stay, she managed to convince the man to give her a couple of rides on the G/Bike, and the two stayed loosely in touch after that.
}