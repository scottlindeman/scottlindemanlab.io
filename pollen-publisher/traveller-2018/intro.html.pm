#lang pollen

◊define-meta[meta-title]{Introduction}
◊(define mc (metas->meta-context select metas))
◊define[pc]{◊(pagetree->pollen-context select)}

◊page[mc]{
From January 2018 through April 2018 a group consisting of myself and four friends put together a ◊ple["https://en.wikipedia.org/wiki/Traveller_(role-playing_game)"]{Traveller} campaign. One of the major decisions was to play something Sci-fi and with great character creation. Traveller definitely has one of the coolest frameworks for this, in which you decide how far you want to go in your character's life, adding events that shape their world view along the way. It is also encouraged to work with other players and collaboratively create past encounters between your characters.

The following is the unfinished, home-rolled campaign I created as the GM. This is written entirely from memory, as 98% of the materials◊hentity{mdash}including the Rulebook◊hentity{mdash}were lost during a move.

Enjoy the tale! And who knows, now that it's written down, we could always pick it back up in the ◊pli['continued.html pc]{future◊hentity{hellip}}
}