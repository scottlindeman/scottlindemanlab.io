#lang pollen

◊define-meta[meta-title]{Return to Englet}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
The four Travellers were been bound by fate to be in need of credits. And so the four of them separately signed on to join a geological excavation expedition funded by the Jenkins Corporation, lending their skills in combat, healing, documentation and command in the name of research.

Starting on the planet of Englet, the newly formed crew met with researchers Dr. Kathy Leonard and Dr. Shoenberg, two geological experts accompanying on the expedition. Leonard, a graduate from a top university on Englet, was brimming with nervous excitement. Schoenberg on the otherhand greeted everyone with suspicion. He seemed to distrust privately funded research expeditions, but was in a similar situation as the Travellers--bills need to get paid.

The trip was a long and uneventful haul, needing to spend some time slipping through space to reach the distant planet of IMP-37698. 
}