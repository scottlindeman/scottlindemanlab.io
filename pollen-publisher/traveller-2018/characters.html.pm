#lang pollen

◊define-meta[meta-title]{Characters}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊page[mc]{
Four Travellers have heard of the promise of the USTs and found their way to the Eastern Marches.
  ◊ol[#:class "sqrnd"]{
    ◊li{
The gonzo journalist and harbinger of death, ◊pli['scratchy.html pc]{}.
    }
    ◊li{
The solemn nomad warrior Aslan, ◊pli['caito.html pc]{}.
    }
    ◊li{
The young pacifist healer, ◊pli['rauler.html pc]{}.
    }
    ◊li{
The irreverent ex-soldier/ex-pirate and general wild-card, ◊pli['lectron.html pc]{}.
    }
  }
}