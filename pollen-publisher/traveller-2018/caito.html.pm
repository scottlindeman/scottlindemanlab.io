#lang pollen

◊define-meta[meta-title]{Ali Caito}
◊(define mc (metas->meta-context select metas))
◊(define pc (pagetree->pollen-context select))

◊define[afirst]{Ali}
◊define[alast]{Caito}

◊define[m]{Mother}
◊define[cfirst]{Castilla}
◊define[pfirst]{Paz}

◊define[la]{Lower Antuas}
◊define[gb]{G/Bike}

◊;Cannie and Lectron

◊page[mc]{
There's a limit to how many miles an Aslan can trek on a ◊gb alone. ◊afirst ◊alast passed that number a long time ago, and did not intend to stop until he found who murdered his mother. Specific images have been ingrained in his mind: black mud-caked boots, a blaster with a unique sigil hand-etched into the hilt, an unmistakable gender-neutral voice. Somehow every step that felt like the right direction always resulted in a dead end, yet he continued on.

◊pimg["static/images/caito-motorcycle.webp" css-page-width "480px"]{
The lonely road.
}

Born in the growing ◊la system to two young Aslans, the bells of opportunity were in his favor. The region had formed a tight inter-planetary alliance, defining new trade routes that would usher in prosperity for the next century. Of course, as trade routes open, so do opportunists looking to ambush unsuspecting traders. The opportunity did not last long.

◊afirst's father passed away early in his infancy from a sickness passing through the ◊la due to the massive influx of immigrants. This left ◊afirst's mother to take on full responsiblity of raising the young Aslan, facing a more and more treacherous world. She did as best she could, many days were hard. When ◊afirst was a teenager, he would spend his afternoons after school helping ◊m with the small mercantile shop she had set up to support the two of them. After shutting down for the night, he'd spend time in the back shed, fixing up his father's old ◊|gb|. The last gift of a man far gone.

As the years went on, ◊m's business did well and ◊afirst finished basic schooling. He decided to forego university to help with ◊m's business. Life was simple, but not without its troubles. Occasionally ◊afirst would step into the storefont to see ◊m being pressed by a "customer". He was big enough to scare the assailant off usually by sight, but that did not make the encounter easier for the two Aslans.

During his time off, ◊afirst was religiously riding his ◊|gb|, and found a group of racers that took the lonely Aslan in: ◊pfirst, Booth and ◊|cfirst|. They were rough around the edges, but the group had heart and moral compass. They were only interested in racing. Similar racing groups often used their ◊gb skills to intercept shipments coming into ◊la and make off with the goods before the system enforcers could arrive. That said, some vessel steerers did not lose their shipment without a fight. ◊afirst had heard of a few riders who had lost their lives in the events.

Of course, ◊m often got herself worried sick about ◊afirst's extracirricular activities. The sound of the ◊gb started to be a warning sign around town. He did his best to stay out of trouble, but the thrill of the wind in his fur as he glided around, racing against the sunsets was too much for him to give up.

He had also began to fall for a member of his racing squad, ◊|cfirst|. He knew there were several layers between them—she didn't seem interested in boys, and a relationship may not even work out between an Aslan and a human, but could not stop the feelings from developing. She rode fast and recklessly, and put a lot of strain on her ◊|gb|. ◊afirst could not race like her, but he could fix up her ride after a particularly tough run.

While working the storefont one day, a ◊pli['lectron.html pc]{Vargyr} he had never seen before walked in. ◊afirst could tell the guest was hiding a limp, this man must have seen a lot in his day. The eye patch, the chunks of missing fur. This was no ordinary vagrant.

The Vargyr introduced himself, and began a conversation that ◊afirst could only describe later as "insane". Beginning by asking way too many personal details about the Aslan's life, he then tried to recruit ◊afirst into starting a band of pirates to reclaim the throne from a close friend who had betrayed him and left him for dead. ◊afirst could not turn away from the story, but he knew he could not leave his mother. The Vargyr sensed the same, and, in parting, said to watch out for the name, what was it again? Lector? Anyway, ◊afirst probably met someone like this once a week in the shop.

Not long after, ◊afirst relayed this encounter with ◊|cfirst|. He was expecting a chuckle out of her, but it was clear she was distracted. Silence permeated the air. Finally, ◊cfirst took ◊afirst's hand and looked him in the eyes. Their squad leader, ◊pfirst was in trouble. He had gotten mixed up with the wrong people and owed them a lot of money. He was going to ambush one of the shipments coming in tonight. ◊afirst knew ◊pfirst could not pull this off alone. He had to stop him.

Before heading out, ◊afirst helped ◊m close up shop for the day. They usered a tall person out of the store so they could clean up before heading home. The person was somewhat unusual, with their face mostly covered and a genderless voice coming from behind the cloth. Finally they were able to get the customer out. During the process, ◊|afirst|'s mother could tell he was distracted, but he would not talk. As ◊afirst quietly said goodbye and headed for the exit, she stopped him.

"◊afirst, before you go, let me tell you a short story about how your father and I met. At eighteen I was languishing. Working at MY parents' diner, not going to school. I was waiting for someone to find me. And one day a gang of ◊|gb|rs rolled into our restaurant. I should have been more nervous than I was, but in our backwaters G-Bikers were a myth. And I'll never forget when your father took off his helmet. Mane falling onto his shoulders... And he saw me, and we couldn't take our eyes off eachother.

"By that evening, my parents knew trouble was afoot. They begged and pleaded, but I snuck out at midnight and hopped on the back of his ◊gb and never looked back. We had you just a few years later, and your father settled down into a more stable lifestyle; we started this shop. But one day an old gang buddy came in, and convinced your father to help with one last job. I pleaded with him not to go, but knew there was nothing I could do to stop his resolve. He did come back a week later, but wherever they went, he came back deathly ill. He lasted maybe one more month, and then the love of my life was gone. And then it was me and you, my dearest one.

"Tonight you have the same look in your eyes that your father did all those years ago. I won't stop you, your resolve is the same as your father's. Just remember you need to come back unscathed. We will deal with anything else as it comes. I don't want to lose my last love."

◊afirst hugged his mother. They stood in silence for a few moments and then ◊afirst took his leave. He kicked the throttle on his ◊gb and blasted off to meet up with ◊|cfirst|. He found her, along with ◊|pfirst|. She was arguing with him, but clearly she wasn't getting through. ◊pfirst rocketed off as soon as ◊afirst dismounted his ◊|gb|, and found himself hopping back on, to give chace with ◊|cfirst|.

Thirty seconds later, the unmistakable sound of charge echoed through the air. ◊afirst and ◊cfirst stopped. Up ahead, smoke. They raced towards it. ◊|pfirst|'s ◊gb was cracked in half, lying on its side. ◊pfirst was fifty meters away, dead. Had his ◊gb been rigged?

Suddenly a chilling voice came from behind ◊|afirst|. It was the same voice as the tall customer he just pushed out of his mother's shop.

"You make my job too easy Aslan. I'm sorry to say, your mother had gotten too suspicious and tried to stop me from following you, so I had to dispose of her before coming over to see if my trap had worked."

They paused, apparently looking at ◊|pfirst|.

"Looks like it did."

◊afirst turned around, not believing what he was hearing. The masked entity's boots were caked in mud, as if they had been working the fields all day. A bespoke blaster encrusted with a sigil was pointed at the two of the ◊|gb|rs.

"My work here is done. Believe me, I don't relish this work, but I'm damn good at it. Don't try anything funny, or else your end up like this poor bastard and your old lady."

Before ◊afirst could think, the androgenyous being hopped on their ◊gb and sped off. It was the last time ◊afirst saw ◊|cfirst|. He didn't even say goodbye as he raced off back to his mother's shop. There she was, just as the entity had proclaimed. ◊afirst dropped to his knees. They would pay. He would scour every planet in the system until he found them. And made them pay.

}