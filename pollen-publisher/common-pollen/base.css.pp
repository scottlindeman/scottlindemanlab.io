#lang pollen

◊(require "../common-rkt/api.rkt")


/* Fundamental browser layouts */
html, body {
  min-height: 100%;
  margin: 0px;
}

body, root {
  display: block;
  height: 100%;
  min-height: 100vh;
}

body {
  margin-bottom: ◊|(css-map-value (λ (v) (- (add1 v))) css-footer-height)|;
}

h1,
h2,
h3,
h4,
h5 {
  margin: 0px;
}

a, a:visited {
  text-decoration: none;
}

ol.sqrnd {
  list-style: none;
  counter-reset: pollen-counter;
}

ol.sqrnd li {
  counter-increment: pollen-counter;
  position: relative;
}

ol.sqrnd li::before {
  content: counter(pollen-counter);
  position: absolute;
  width: 40px;
  height: 40px;
  line-height: 42px;
  left: -58px;
  top: 19px;
  text-align: center;
  font-weight: bold;
  font-size: 40px;
  border: 4px solid ◊|(css-colors-black css-sl-colors)|;
  border-radius: 25%;
}

/* Common Classes  */
.no-hyphens {
  hyphens: none;
  -webkit-hyphens: none;
}

/* Book Elements */

/* Book */
.pollen-book {
  margin: auto;
  max-width: ◊|css-book-width|;
  height: 100%;
}

/* Next Page, Previous Page */
.page-next, .page-prev {
  height: 100%;
  width: 50px;
  position: fixed;
  cursor: pointer;
  display: flex;
}

.page-next > span, .page-prev > span {
  display: none;
  font-size: 50px;
  text-align: center;
  flex-direction: column;
  justify-content: center;
  margin-left: 30%;
}
.page-next:hover > span, .page-prev:hover > span {
  display: flex;
}
.page-next {
  right: 0px;
}
.page-prev {
  left: 0px;
}

/* Title and Table of Contents */

.book-title {
  width: ◊|css-book-title-width|;
  height: 100%;
  position: fixed;
}

.table-of-contents {
  width: ◊|css-toc-width|;
  margin-left: ◊|css-book-title-width|;
}

.table-of-contents a {
  padding: 10px;
  display: block;
}

/* Footer */
.footer {
  margin: auto;
  width: ◊|css-book-width|;
  height: ◊|css-footer-height|;
}
.footer a, .footer a:visited {
  display: block;
  text-align: center;
}

.footer-top {
  text-align: center;
}
.footer-prev {
  text-align: left;
}
.footer-next {
  text-align: right;
  padding-bottom: 50px;
}
.footer-prev, .footer-next, .footer-top {
  width: 100%;
  padding-top: 30px;
}

/* Page */
.page {
  padding-left: 50px;
  padding-right: 50px;
}

.page-title {
  margin-bottom: 50px;
}
.page.right {
  margin-left: ◊|css-page-runner-width|;
}
.page p, .page li {
  width: 100%;
  hyphens: auto;
}

.page-link, .page-link:visited {
  text-transform: uppercase;
  padding-left: 5px;
  padding-right: 5px;
  letter-spacing: 1px;
}

.page-image-link {
  display: block;
}

img.page-image {
  border-radius: 5px;
}

/* Media Query for non-mobile pages */
◊css-media-width {
  ol.sqrnd li::before {
    width: 20px;
    height: 20px;
    line-height: 21px;
    left: -40px;
    top: 5px;
    font-size: 18px;
    border: 2px solid ◊|(css-colors-black css-sl-colors)|;
  }

  .page-next, .page-prev {
    width: 100px;
  }

  .page {
    width: ◊|css-page-width|;
    padding: 0px;
  }

  .footer {
    display: flex;
  }

  .footer-prev, .footer-next, .footer-top {
    width: 33%;
  }

  .footer-next {
    padding-bottom: 0px;
  }

}
