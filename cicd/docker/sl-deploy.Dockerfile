FROM node:16.15-bullseye-slim

ARG RACKET_VERSION

RUN apt update \
    && apt install -y --no-install-recommends \
    sudo \
    wget \
    ca-certificates \
    sqlite3 \
    && apt clean

RUN wget --no-check-certificate "https://download.racket-lang.org/releases/${RACKET_VERSION}/installers/racket-${RACKET_VERSION}-x86_64-linux-cs.sh" -O /tmp/racket-install.sh \
    && sh /tmp/racket-install.sh --in-place --dest /opt/racket --create-links /usr/local \
    && rm /tmp/racket-install.sh

ENV SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt"
ENV SSL_CERT_DIR="/etc/ssl/certs"

RUN raco setup
RUN raco pkg config --set catalogs \
    "https://download.racket-lang.org/releases/${RACKET_VERSION}/catalog/" \
    "https://pkg-build.racket-lang.org/server/built/catalog/" \
    "https://pkgs.racket-lang.org" \
    "https://planet-compats.racket-lang.org"
