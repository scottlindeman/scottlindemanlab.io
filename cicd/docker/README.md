# Docker

## Build
Building is done manually at the moment.

### sl-deploy

    # Top level, Racket 8.5
    > TAG_VERSION=$(cat cicd/docker/versions/SL-DEPLOY) docker build --build-arg RACKET_VERSION="8.5" -f cicd/docker/sl-deploy.Dockerfile -t scottlindeman/sl-deploy:${TAG_VERSION} .
