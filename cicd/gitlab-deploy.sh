#! /usr/bin/env bash

set -e
set -u

POLLEN_DIR="pollen-publisher"
PUBLIC_POLLEN_DIR="public/tabletop-books"

function install_env {
    echo "[BUILD] Installing npm environment"
    npm install

    echo "[BUILD] Installing racket environment"
    local CURRENT_DIR=$(pwd)
    cd ${POLLEN_DIR}
    raco pkg install -D --auto
    cd ${CURRENT_DIR}
}

function build_ui {
    echo "[BUILD] Buliding the UI"
    npm run build
}

function build_pollen {
    local BOOKS=("traveller-2018" "ryuutama-2021")

    for b in ${BOOKS[*]}; do
        echo "[BUILD] Building Pollen book ${b}"
        local SRC_BOOK_PATH="${POLLEN_DIR}/${b}"
        local DEST_BOOK_PATH="${PUBLIC_POLLEN_DIR}/${b}"

        mkdir -p ${DEST_BOOK_PATH}

        raco pollen render ${SRC_BOOK_PATH}
        raco pollen publish ${SRC_BOOK_PATH} ${DEST_BOOK_PATH}
    done
}

install_env
build_ui
build_pollen
