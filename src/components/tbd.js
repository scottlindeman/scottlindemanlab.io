import m from "mithril";

import TopBlurb from "./top-blurb.js";
import Description from "./description.js";

/*
 * This one has some layout attributes that are worth documenting
 * - shorten - Box the content into a 1-3 grid, so it doesn't exend the whole width
 * - svg - A Component of an svg to render next to the blurb and description
 * - swap - Default is svg on the right, swap=true puts svg on left
 * - blurb - Text for the blurb, just a string
 * - desc - Text for the description, usually list of string
 */
const TBDCombo = {
  view: (vnode) => {
    if (vnode.attrs.svg || vnode.attrs.shorten) {
      const contentList = [];

      const tbd = m(".pure-u-2-3.pure-g.shortened", [
        m(TopBlurb, vnode.attrs.blurb),
        m(Description, { sentences: vnode.attrs.sentences }),
      ]);

      const svg = m(".pure-u-1-3", vnode.attrs.svg);

      if (vnode.attrs.svg && vnode.attrs.swap) {
        contentList.push(svg);
        contentList.push(tbd);
      } else {
        contentList.push(tbd);

        if (vnode.attrs.svg) {
          contentList.push(svg);
        }
      }

      return contentList;
    } else {
      return [
        m(TopBlurb, vnode.attrs.blurb),
        m(Description, { sentences: vnode.attrs.sentences }),
      ];
    }
  },
};

export default TBDCombo;
