import m from "mithril";

const PagesEnum = {
  home: {
    routeName: "/",
    headerName: "Home",
  },
  now: {
    routeName: "/now",
    headerName: "Now",
  },
  books: {
    routeName: "/books",
    headerName: "Books",
  },
  bestof: {
    routeName: "/best-of",
    headerName: "Best Of",
  },
  projects: {
    routeName: "/projects",
    headerName: "Projects",
  },
  thoughts: {
    routeName: "/thoughts",
    headerName: "Thoughts",
  },
  credits: {
    routeName: "/credits",
    headerName: "Credits",
  },
};

const Header = {
  view: function (vnode) {
    let selectedPage = vnode.attrs.page;

    if (!selectedPage) {
      selectedPage = PagesEnum.home;
    }

    const pages = Object.keys(PagesEnum).map((k) => {
      const page = PagesEnum[k];
      const attrs = {
        href: page.routeName,
        class: "nav-link",
      };
      const content = [m("span", page.headerName)];

      const isSelected = page.routeName === selectedPage.routeName;

      if (isSelected) {
        attrs.class += " selected";
        content.push(m("br"));
        content.push(m("span", "•"));
      }

      return m(m.route.Link, attrs, content);
    });

    return m(".sl-navbar", [
      m("h1", m(m.route.Link, { href: PagesEnum.home.routeName }, "SL")),
      m(".nav-centered", [pages]),
    ]);
  },
};

export { Header, PagesEnum };
