import m from "mithril";

import Clock from "./clock.js";
import MeNCat from "../images/mencat.png";

const text1 =
  "A bit about myself: I am a software developer by trade. This website is a collection of things I have created over the years along with some thoughts about our journey here on Earth. Maybe you'll find something interesting, but mainly I built this as a journal for myself.";

const Home = {
  view: () => {
    return m(".home.sl-pane", [
      m(Clock),
      m(".pure-u-1-1 .pure-u-lg-1-2", [m("p", "Welcome."), m("p", text1)]),
      m(
        ".pure-u-1-1 .pure-u-lg-1-2",
        m("img.me", { src: MeNCat, width: "80%", title: "Me and Cat" })
      ),
    ]);
  },
};

export default Home;
