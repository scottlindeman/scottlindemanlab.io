import m from "mithril";

import "../style/home.css";
import TheDay from "../images/theday.png";

import TopBlurb from "./top-blurb.js";
import DateService from "../services/date.js";

const timeUpdater = (vnode) => {
  return setInterval(() => {
    const newTime = DateService.now();
    vnode.state.timeOfDay = DateService.timeOfDay(newTime);
    vnode.state.time = DateService.dateToTime(newTime);
    vnode.state.date = DateService.dateToDate(newTime);
    vnode.state.backgroundY = backgroundY(newTime);

    document.getElementsByClassName("top-blurb")[0].innerHTML =
      vnode.state.timeOfDay;
    document.getElementsByClassName("clock")[0].style["background-position-y"] =
      vnode.state.backgroundY;
    document.getElementsByClassName("clock-date")[0].innerHTML =
      vnode.state.date;
    document.getElementsByClassName("clock-time")[0].innerHTML =
      vnode.state.time;
    // Note, I could not for the life of me get m.redraw()
    // working here, even though I had the same exact code as
    // online examples. Having to use a #DIRTYHACK with
    // getElementById.
  }, 1000);
};

const backgroundY = (d) => {
  const pos = parseInt(DateService.percentageOfDay(d) * 924);
  return `${pos + 120}px`;
};

const Clock = {
  oninit: (vnode) => {
    const initTime = DateService.now();
    vnode.state.timeOfDay = DateService.timeOfDay(initTime);
    vnode.state.backgroundY = backgroundY(initTime);
    vnode.state.time = DateService.dateToTime(initTime);
    vnode.state.date = DateService.dateToDate(initTime);
    vnode.state.interval = timeUpdater(vnode);
  },
  view: (vnode) => {
    return [
      m(TopBlurb, vnode.state.timeOfDay),
      m(
        ".clock.pure-u-1-1",
        {
          style: `background-image: url(${TheDay}); background-position-y: ${vnode.state.backgroundY}`,
        },
        [
          m("h2.clock-date", vnode.state.date),
          m("span.clock-time.mono", vnode.state.time),
        ]
      ),
    ];
  },
  onremove: (vnode) => {
    clearInterval(vnode.state.interval);
  },
};

export default Clock;
