import m from "mithril";
import "../style/now.css";
import DateService from "../services/date.js";
import TBDCombo from "./tbd.js";

import NowA from "../images/now1.jpg";
import NowB from "../images/now2.jpg";
import NowC from "../images/now3.jpg";

const LastUpdateRaw = 1696921501984; // Just call Date.now() whenever you update.

const Now = {
  view: () => {
    const lastUpdateDate = new Date(LastUpdateRaw);

    const Updates = [
      "Currently in British Columbia, but heading back to the US in a few weeks. Canada has been wonderful and the time from late Spring to early Fall here in B.C. has been absolutely beautiful.",
      [
        "I've been focusing on playing and recording music, both with a band and producing via ",
        m("a", { href: "https://www.reaper.fm/", target: "_blank" }, "Reaper"),
        ". It's be fun to learn all these tools, and slowly get better at them. Slowly.",
      ],
      [
        "I've been working on some very early company ideas with former colleagues, and on my own. We will see where this all goes. Otherwise, I've gone deep down the rabbit hole of having a small ecosystem of self-deployed services to house my online presense. This website, plus systems like ",
        m(
          "a",
          { href: "https://funkwhale.audio/", target: "_blank" },
          "Funkwhale"
        ),
        " and ",
        m(
          "a",
          { href: "https://joinmastodon.org/", target: "_blank" },
          "Mastodon"
        ),
        ".",
      ],
      "Coding is still fun. Music is still fun. I'd like to keep at both if possible.",
    ];

    const Gallery = [
      { image: NowA, caption: "Just outside Banff, in the early October air." },
      {
        image: NowB,
        caption: "A view of fall mountain colors in northern Banff.",
      },
      { image: NowC, caption: "Mt. Rundle in the mist." },
    ];

    return m(".now.sl-pane", [
      m(TBDCombo, {
        blurb: "What am I up to now?",
        sentences: [
          `Last updated on ${DateService.dateToDate(lastUpdateDate, true)}.`,
        ],
      }),
      m(
        ".web.pure-u-1-1.pure-u-lg-1-2",
        Updates.map((t) => m("p", t))
      ),
      m(
        ".web.pure-u-1-1.pure-u-lg-1-2",
        Gallery.map((i) => {
          return m(".with-caption", [
            m("img", { src: i.image, alt: i.caption }),
            m("br"),
            m("span", i.caption),
          ]);
        })
      ),
    ]);
  },
};

export default Now;
