import m from "mithril";

import NavService from "../../services/nav.js";

const setAnchor = (anchorName) => {
  return () => {
    const lastRoute = m.route.get();

    m.route.set(lastRoute.split("?")[0], { name: anchorName });
  };
};

const Section = {
  view: (vnode) => {
    const anchorName = NavService.createAnchor(vnode.attrs.t);
    const contentList = [
      m("h2.title.pure-u-1-1", { name: anchorName }, [
        vnode.attrs.t,
        m(
          "span.anchor-icon",
          {
            onclick: setAnchor(anchorName),
          },
          "#"
        ),
      ]),
    ];

    let pureSize = ".pure-u-1-1";

    if (vnode.attrs.s) {
      contentList.push(m(`.subtitle${pureSize}`, vnode.attrs.s));
    }

    let listTag = "ul";
    if (vnode.attrs.o) {
      listTag = "ol";
    }

    contentList.push(m(`${listTag}.content${pureSize}`, vnode.children));

    return m(".section.pure-u-1-1.pure-g", contentList);
  },
};

const Item = {
  view: (vnode) => {
    const basicText = vnode.attrs.bt;
    const longerText = vnode.attrs.ct;

    const contentList = [];
    contentList.push(basicText);

    if (longerText) {
      contentList.push(m(".commentary", longerText));
    }

    return m("li", contentList);
  },
};

export { Section, Item };
