import m from "mithril";
import "../../style/bestof.css";
import {
  BGOne,
  BGTwo,
  BGThree,
  BGFour,
  Music,
  Web,
  Idea,
  Travel,
} from "./icons.js";

import NavService from "../../services/nav.js";

import { PagesEnum } from "../header.js";
import TBDCombo from "../tbd.js";
import TopBlurb from "../top-blurb.js";
import Description from "../description.js";

import BestOfList from "./list.js";

/* --- Pages ---------------------------------- */

const BestOfPagesEnum = {
  inspirations: {
    routeName: `${PagesEnum.bestof.routeName}/inspirations`,
    name: "Inspirations",
    svg: BGTwo,
    category: Idea,
    description: "People, content and things that provide me inspiration.",
  },
  albums: {
    routeName: `${PagesEnum.bestof.routeName}/albums`,
    name: "Albums",
    svg: BGOne,
    category: Music,
    description: "Some of my favorite albums, by year.",
  },
  websites: {
    routeName: `${PagesEnum.bestof.routeName}/websites`,
    name: "Websites",
    svg: BGThree,
    category: Web,
    description: "A collection of my favorite websites around the internet.",
  },
  symphonies: {
    routeName: `${PagesEnum.bestof.routeName}/symphonies`,
    name: "Symphonies",
    svg: BGTwo,
    category: Music,
    description:
      "Various lists about favorite symphonies, favorite symphonists, etc.",
  },
  recipes: {
    routeName: `${PagesEnum.bestof.routeName}/recipes`,
    name: "Recipes",
    svg: BGFour,
    category: Idea, //Should be food?
    description: "Some recipes I've really liked over the years.",
  },
  soundtracks: {
    routeName: `${PagesEnum.bestof.routeName}/soundtracks`,
    name: "Soundtracks",
    svg: BGThree,
    category: Music,
    description: "Film and video game original soundtracks",
  },
  travelgear: {
    routeName: `${PagesEnum.bestof.routeName}/travelgear`,
    name: "Travel_Gear",
    svg: BGOne,
    category: Travel,
    description: "The essentials for travel",
  },
};

/* --- Components ----------------------------- */

const BestOf = {
  view: () => {
    const pages = Object.keys(BestOfPagesEnum).map((k) => {
      return m(BestOfLink, { bestOfPage: BestOfPagesEnum[k] });
    });

    return m(
      ".bestof.sl-pane",
      [
        m(TBDCombo, {
          blurb: "Who doesn't like lists?",
          sentences: [
            "I find myself creating lots of lists, maybe more than I should.",
            "Playlists, Albums of the Year, Movies, Books, Concerts, Wishlists, etc.",
            "You'll find some of them here.",
            "Not all of them are rankings, of course.",
          ],
        }),
      ].concat(pages)
    );
  },
};

const BestOfLink = {
  view: (vnode) => {
    const page = vnode.attrs.bestOfPage;

    return m(
      ".bestof-link.pure-u-1-1.pure-u-lg-1-2.pure-g",
      m(m.route.Link, { href: page.routeName }, [
        m(page.svg, { name: page.name }, m(page.category)),
        m(".bestof-blurb.pure-u-3-5.pure-u-lg-1-3.pure-g", [
          m("h3.bestof-title.pure-u-1-1", page.name.replace("_", " ")),
          m("p.pure-u-1-1", page.description),
        ]),
      ])
    );
  },
};

const BestOfLayout = {
  onupdate: (vnode) => {
    NavService.scrollToAnchor(m.route.param("name"));
  },
  view: (vnode) => {
    const page = vnode.attrs.page;

    return m(".bestof-page.sl-pane", [
      m(
        "h2.back.pure-u-1-1",
        m(
          m.route.Link,
          { href: PagesEnum.bestof.routeName },
          "❰ Back to Best Of"
        )
      ),
      m(TBDCombo, {
        blurb: page.name.replace("_", " "),
        sentences: [page.description],
        svg: m(page.svg, { name: page.name }, m(page.category)),
      }),
      m(".bestof-list.pure-g", m(BestOfList, { name: page.name })),
    ]);
  },
};

export { BestOf, BestOfLayout, BestOfPagesEnum };
