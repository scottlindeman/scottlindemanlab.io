import m from "mithril";

import { Section, Item } from "./page.js";

import handleMarkdown from "../../services/markdown.js";
import DocService from "../../services/docs.js";

const BestOfList = {
  oninit: (vnode) => {
    DocService.getList(vnode.attrs.name, (result) => {
      vnode.state.document = result;
    });
  },
  view: (vnode) => {
    if (!vnode.state.document) {
      return m(".oops");
    }
    const sections = DocService.splitIntoSections(vnode.state.document);
    return sections.map((s) => {
      const lines = DocService.splitIntoLines(s);
      const title = lines.shift().substring(1);
      let subtitle;
      if (lines && !lines[0].startsWith("- ")) {
        subtitle = lines.shift();
      }
      const items = [];

      for (let i = 0; i < lines.length; i++) {
        let line = lines[i];

        if (!line) {
          break;
        }

        line = handleMarkdown(line.substring(2));

        const nextLine = lines[i + 1];

        if (!nextLine || (nextLine && nextLine.startsWith("- "))) {
          items.push(m(Item, { bt: line }));
        } else if (nextLine) {
          const commentary = handleMarkdown(lines[i + 1]);
          items.push(m(Item, { bt: line, ct: commentary }));
          i += 1;
        }
      }

      return m(Section, { t: title, s: subtitle }, items);
    });
  },
};

export default BestOfList;
