import m from "mithril";
import "../style/credits.css";
import ColorScheme from "../images/color-scheme.jpg";
import MithrilLogo from "../images/mithril-logo.svg";

import TopBlurb from "./top-blurb.js";

const Credits = {
  view: () => {
    return m(".credits.sl-pane", [
      m(TopBlurb, "Credit given where credit deserved."),
      m(".web.pure-u-1-1.pure-u-lg-1-2", [
        m("p.mithril", [
          "Built with ",
          m("img", { src: MithrilLogo, height: "16", width: "16" }),
          " ",
          m(
            "a",
            { href: "https://mithril.js.org/", target: "_blank" },
            "Mithril"
          ),
          " and ",
          m(
            "a",
            { href: "https://purecss.io/grids/", target: "_blank" },
            "Pure Grid"
          ),
          ".",
        ]),
        m("p.typefaces", [
          "Set in the ",
          m(
            "a.oxy",
            {
              href: "https://fonts.google.com/specimen/Oxygen",
              target: "_blank",
            },
            "Oxygen"
          ),
          ", ",
          m(
            "a",
            {
              href: "https://fonts.google.com/specimen/Fira+Sans",
              target: "_blank",
            },
            "Fira Sans"
          ),
          " and ",
          m(
            "a.mono",
            {
              href: "https://fonts.google.com/specimen/Fira+Mono",
              target: "_blank",
            },
            "Fira Mono"
          ),
          " typefaces.",
        ]),
        m("p.tools", [
          "Large icon svgs inspired by ",
          m(
            "a",
            {
              href: "https://www.jetbrains.com/company/brand/logos/",
              target: "_blank",
            },
            "JetBrains Logos"
          ),
          " (no affiliation). The clock background was inspired by ",
          m(
            "a",
            {
              href: "https://tips.clip-studio.com/en-us/articles/3567",
              target: "_blank",
            },
            "an article by Beanpots"
          ),
          ". Most designs were created with a combination of ",
          m(
            "a",
            { href: "https://editor.method.ac/", target: "_blank" },
            "Method Draw Vector Editor"
          ),
          ", ",
          m("a", { href: "https://inkscape.org/" }, "Inkscape"),
          " and ",
          m("a", { href: "https://www.gimp.org/" }, "GIMP"),
          ".",
        ]),
        m("p.source", [
          "The source code for this website can be found on ",
          m(
            "a",
            {
              href: "https://gitlab.com/scottlindeman/scottlindeman.gitlab.io",
              target: "_blank",
            },
            "GitLab."
          ),
        ]),
      ]),
      m(".colors.pure-u-1-1.pure-u-lg-1-2", [
        m("p.intro", [
          "Color palette inspired by a plate from ",
          m(
            "a",
            {
              href: "https://www.sacredbonesrecords.com/products/emily-noyes-vanderpoel-color-problems",
              target: "_blank",
            },
            "Color Problems"
          ),
          " by Emily Noyes Vanderpoel.",
        ]),
        m("img", { src: ColorScheme }),
      ]),
    ]);
  },
};

export default Credits;
