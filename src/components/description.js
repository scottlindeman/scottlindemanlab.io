import m from "mithril";

const Description = {
  view: (vnode) => {
    return m(
      ".description.pure-u-1-1",
      vnode.attrs.sentences.map((s) => {
        return m("p", s);
      })
    );
  },
};

export default Description;
