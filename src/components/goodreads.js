import m from "mithril";
import "../style/goodreads.css";

import TBDCombo from "./tbd.js";

const ReadJSLink =
  "https://www.goodreads.com/review/custom_widget/131051733.Recently%20Read?cover_position=left&cover_size=medium&num_books=10&order=d&shelf=read&show_author=1&show_cover=1&show_rating=1&show_review=0&show_tags=0&show_title=1&sort=date_read&widget_id=1623516086";

const UpcomingJSLink =
  "https://www.goodreads.com/review/custom_widget/131051733.Next%20Up?cover_position=left&num_books=10&order=d&shelf=to-read&show_author=1&show_cover=1&show_rating=0&show_review=0&show_tags=0&show_title=1&sort=author&widget_id=1623380747";

const UpNextJSLink =
  "https://www.goodreads.com/review/custom_widget/131051733.Up%20Next?cover_position=left&cover_size=medium&num_books=10&order=d&shelf=to-read&show_author=1&show_cover=1&show_rating=0&show_review=0&show_tags=0&show_title=1&sort=author&widget_bg_color=FFFFFF&widget_bg_transparent=true&widget_border_width=1&widget_id=1623516108&widget_text_color=000000&widget_title_size=medium&widget_width=medium";

const GoodReads = {
  view: () => {
    return m(".goodreads.sl-pane", [
      m(TBDCombo, {
        blurb: "Continued education.",
        sentences: [
          "I try my best to read 12 books a year, 6 fiction and 6 non-fiction.",
          "Some years I can't achieve this goal, some years I am well over.",
        ],
      }),
      m(GoodReadsRead),
      m(GoodReadsToRead),
    ]);
  },
};

const GoodReadsRead = {
  view: () => {
    return m(".goodreads-read.pure-u-1-1.pure-u-lg-1-2", [
      m("#gr_custom_widget_1623516086"),
      m("script", {
        src: ReadJSLink,
        type: "text/javascript",
        charset: "utf-8",
      }),
    ]);
  },
};

const GoodReadsToRead = {
  view: () => {
    return m(".goodreads-unread.pure-u-1-1.pure-u-lg-1-2", [
      m("#gr_custom_widget_1623516108"),
      m("script", {
        src: UpNextJSLink,
        type: "text/javascript",
        charset: "utf-8",
      }),
    ]);
  },
};

export default GoodReads;
