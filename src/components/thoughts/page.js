import m from "mithril";

import Illuminated from "./icons.js";

import handleMarkdown from "../../services/markdown.js";
import DateService from "../../services/date.js";
import DocService from "../../services/docs.js";

const shortenedIndices = (sections) => {
  let charLimit = 1250;
  const indices = [];
  for (let i = sections.length - 1; i >= 0; i--) {
    const s = sections[i];
    charLimit = charLimit - s.length;
    indices.push(i);
    if (charLimit <= 0) {
      break;
    }
  }
  return indices;
};

const ThoughtPage = {
  oninit: (vnode) => {
    DocService.getThought(vnode.attrs.thought.id, (result) => {
      vnode.state.document = result;
    });
  },
  view: (vnode) => {
    if (!vnode.state.document) {
      return m(".oops");
    }
    const sections = DocService.splitIntoSections(vnode.state.document);
    const first = sections.shift();
    const letter = first[0];
    const firstPeriod = first.indexOf(". ");
    const restOfFirstSentence = first.substring(1, firstPeriod + 1);
    const restOfFirstParagraph = first.substring(firstPeriod + 2);

    const shortened = shortenedIndices(sections);

    return [
      m(".read-time", DocService.timeToRead(vnode.state.document)),
      m(".pure-u-1-1.pure-g", [
        m(".pure-u-1-3.illuminated-box", m(Illuminated, { letter: letter })),
        m(".pure-u-2-3", [
          m("h1.first-line", restOfFirstSentence),
          m("p.first-paragraph", handleMarkdown(restOfFirstParagraph)),
        ]),
      ]),
      sections.map((s, sidx) => {
        let pureClass = ".pure-u-4-5";
        const shortIdx = shortened.indexOf(sidx);
        if (shortIdx >= 0) {
          pureClass = ".pure-u-2-3";
          if (shortIdx === shortened.length - 1) {
            pureClass = ".pure-u-3-4";
          }
        }
        if (s.startsWith("#")) {
          return m(`h2.nth${pureClass}`, s.substring(1));
        } else {
          return m(`p.nth${pureClass}`, handleMarkdown(s));
        }
      }),
      m("p.nth.signature.pure-u-3-4.pure-g", [
        m("h3.pure-u-1-2", "- SL"),
        m(
          "h3.date.pure-u-1-2",
          DateService.dateToDate(
            DateService.createDate(vnode.attrs.thought.date),
            true
          )
        ),
      ]),
    ];
  },
};

export default ThoughtPage;
