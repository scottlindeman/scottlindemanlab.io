import m from "mithril";
import "../../style/thoughts.css";
import IlluminatedBR from "../../images/illuminated-br.png";

import TBDCombo from "../tbd.js";
import TopBlurb from "../top-blurb.js";
import { PagesEnum } from "../header.js";
import DateService from "../../services/date.js";

import ThoughtPage from "./page.js";

const ThoughtsMetadata = {
  1: {
    id: 1,
    title: "Mission Statement",
    date: "2021-06-13",
  },
  2: {
    id: 2,
    title: "Basketball is my Favorite Sport",
    date: "2022-05-28",
  },
  3: {
    id: 3,
    title: "Coding Culture Wars",
    date: "2022-05-31",
  },
  // 4: {
  //   id: 4,
  //   title: "Software Testing and Machine Learning",
  //   date: "2020-05-29",
  // },
};

const Thoughts = {
  organizedThoughts: () => {
    const thoughtsByYear = {};
    const thoughts = Object.values(ThoughtsMetadata);

    thoughts.forEach((t) => {
      const y = DateService.createDate(t.date).getFullYear().toString();
      console.log(typeof y);
      if (thoughtsByYear[y]) {
        thoughtsByYear[y].push(t);
      } else {
        thoughtsByYear[y] = [t];
      }
    });
    return thoughtsByYear;
  },
  view: () => {
    const thoughts = Thoughts.organizedThoughts();
    const sortedYears = Object.keys(thoughts).sort((a, b) => {
      return b - a;
    });
    const thoughtsM = sortedYears.map((y) => {
      return [
        m(".thoughts-list-title.pure-u-1-1.pure-g", m("h2.pure-u-5-24", y)),
      ].concat(
        thoughts[y].sort(DateService.compareDates(true, true)).map((t) => {
          return m(ThoughtsLink, { thought: t });
        })
      );
    });

    return m(".thoughts.sl-pane", [
      m(TBDCombo, {
        blurb: "Unsolicited, always.",
        sentences: ["All opinions expressed here are mine alone.", "Enjoy!"],
      }),
      thoughtsM,
    ]);
  },
};

const CoolDate = {
  view: (vnode) => {
    const date = DateService.createDate(vnode.attrs.dateString);

    return m(".cool-date.pure-g.oxy", [
      m(
        "span.pure-u-1-1.month",
        date.toLocaleString("en-us", { month: "short" })
      ),
      m("span.pure-u-1-1.day", date.getDate()),
      m("span.pure-u-1-1.year", date.getFullYear()),
    ]);
  },
};

const TitleLength = 18;

const ThoughtsLink = {
  view: (vnode) => {
    const t = vnode.attrs.thought;
    let title = t.title;
    if (title.length > TitleLength) {
      title = title.substring(0, TitleLength) + "...";
    }

    return m(
      ".thought-link.pure-u-1-2.pure-u-lg-1-3.pure-g",
      m(
        m.route.Link,
        {
          href: `${PagesEnum.thoughts.routeName}/${t.id}`,
          title: t.title,
        },
        [
          m("span.pure-u-1-8", m(CoolDate, { dateString: t.date })),
          m("h3.pure-u-4-5", title),
        ]
      )
    );
  },
};

const ThoughtsLayout = {
  view: (vnode) => {
    const thought = vnode.attrs.thought;

    return m(
      ".thoughts-page.sl-pane.oxy",
      { style: `background-image: url(${IlluminatedBR})` },
      [
        m(
          "h2.back.pure-u-1-1",
          m(
            m.route.Link,
            { href: PagesEnum.thoughts.routeName },
            "❰ Back to Thoughts"
          )
        ),
        m(TopBlurb, thought.title),
        m(ThoughtPage, { thought: thought }),
      ]
    );
  },
};

export { Thoughts, ThoughtsLayout, ThoughtsMetadata };
