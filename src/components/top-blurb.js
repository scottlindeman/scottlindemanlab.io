import m from "mithril";

const TopBlurb = {
  view: (vnode) => {
    return m("h2.top-blurb.pure-u-1-1", vnode.children);
  },
};

export default TopBlurb;
