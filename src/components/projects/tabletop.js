import m from "mithril";

import "../../style/projects.css";

import { PagesEnum } from "../header.js";
import TBDCombo from "../tbd.js";

const Tabletop = {
  view: (vnode) => {
    return m(".projects-page.sl-pane", [
      m(
        "h2.back.pure-u-1-1",
        m(
          m.route.Link,
          { href: PagesEnum.projects.routeName },
          "❰ Back to Projects"
        )
      ),
      m(TBDCombo, {
        blurb: vnode.attrs.project.name,
        sentences: [vnode.attrs.project.description],
        svg: m(".project-icon", {
          style: `background-image: url(${vnode.attrs.project.svg}); height: 300px;`,
        }),
      }),
      m("ul.tabletop-list.pure-u-1-1", [
        m(
          "h3",
          m(
            "a",
            {
              href: "tabletop-books/traveller-2018/index.html",
              target: "_blank",
            },
            "Traveller 2018 Campaign"
          )
        ),
        m("p", "Homebrew Traveller setting."),
        m(
          "h3",
          m(
            "a",
            {
              href: "tabletop-books/ryuutama-2021/index.html",
              target: "_blank",
            },
            "Ryuutama 2021 Campaign"
          )
        ),
        m("p", "Homebrew Ryuutama setting."),
      ]),
    ]);
  },
};

export default Tabletop;
