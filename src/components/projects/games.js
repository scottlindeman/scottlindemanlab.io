import m from "mithril";

import "../../style/projects.css";

import { PagesEnum } from "../header.js";
import TBDCombo from "../tbd.js";

const Games = {
  view: (vnode) => {
    return m(".projects-page.sl-pane", [
      m(
        "h2.back.pure-u-1-1",
        m(
          m.route.Link,
          { href: PagesEnum.projects.routeName },
          "❰ Back to Projects"
        )
      ),
      m(TBDCombo, {
        blurb: vnode.attrs.project.name,
        sentences: [vnode.attrs.project.description],
        svg: m(".project-icon", {
          style: `background-image: url(${vnode.attrs.project.svg}); height: 300px`,
        }),
      }),
      m("ul.games-list.pure-u-1-1", [
        m(
          "h3",
          m(
            "a",
            { href: "http://doublesltba.gitlab.io/dsltba", target: "_blank" },
            "DoubleSL Text Based Adventure"
          )
        ),
        m("p", "A choose your own adventure system."),
	m(
          "h3",
          m(
            "a",
            { href: "https://bajathefrog.itch.io/jouncer-px", target: "_blank" },
            "Jouncer PX"
          )
        ),
        m("p", "Created the music and sound effects for this one button arcade game."),
	m(
          "h3",
          m(
            "a",
            { href: "https://ldjam.com/events/ludum-dare/55/ghostdusters", target: "_blank" },
            "Ghostdusters"
          )
        ),
        m("p", "Created the music and sound effects for Ludum Dare game jam entry."),
      ]),
    ]);
  },
};

export default Games;
