import m from "mithril";

import "../../style/projects.css";
import PA from "../../images/project-a.svg";
import PB from "../../images/project-b.svg";
import PC from "../../images/project-c.svg";
import PD from "../../images/project-d.svg";

import { PagesEnum } from "../header.js";
import TBDCombo from "../tbd.js";

const ProjectsPagesEnum = {
  tabletop: {
    routeName: `${PagesEnum.projects.routeName}/tabletop`,
    name: "Tabletop",
    description: "Materials from various campaigns I've run or been a part of.",
    svg: PA,
    orient: "left",
  },
  maps: {
    routeName: `${PagesEnum.projects.routeName}/maps`,
    name: "Maps",
    description:
      "Odes to various places around the world that have inspired me.",
    svg: PB,
    orient: "right",
  },
  games: {
    routeName: `${PagesEnum.projects.routeName}/games`,
    name: "Games",
    description: "A short list of games I have helped build.",
    svg: PC,
    orient: "left",
  },
};

const Projects = {
  view: () => {
    const pls = Object.keys(ProjectsPagesEnum).map((k) => {
      return m(ProjectLink, { projectPage: ProjectsPagesEnum[k] });
    });

    return m(".projects.sl-pane", [
      m(TBDCombo, {
        blurb: "Digital Legacy.",
        sentences: [
          "A collection of work I am happy to show off.",
          "Some of it I consider quite good, some not too bad.",
          "Judge for yourself.",
        ],
      }),
      pls,
    ]);
  },
};

const ProjectLink = {
  view: (vnode) => {
    const pPage = vnode.attrs.projectPage;

    const img = m(".project-icon.pure-u-1-3", {
      style: `background-image: url(${pPage.svg})`,
    });
    const text = m(".pure-u-2-3", [
      m("h2.project-name", pPage.name),
      m("p.project-description", pPage.description),
    ]);

    let content = [];
    if (pPage.orient === "left") {
      content.push(img);
      content.push(text);
    } else {
      content.push(text);
      content.push(img);
    }

    return m(
      ".projects-link.pure-u-1-1",
      m(
        ".project-info.pure-g",
        m(m.route.Link, { href: pPage.routeName }, content)
      )
    );
  },
};

export { Projects, ProjectsPagesEnum };
