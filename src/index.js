import m from "mithril";
import "./style/style.css";
import "./style/pure-grid.css";

import { Header, PagesEnum } from "./components/header.js";
import Home from "./components/home.js";
import Now from "./components/now.js";
import GoodReads from "./components/goodreads.js";
import Credits from "./components/credits.js";
import {
  BestOf,
  BestOfLayout,
  BestOfPagesEnum,
} from "./components/bestof/best-of.js";
import {
  Thoughts,
  ThoughtsLayout,
  ThoughtsMetadata,
} from "./components/thoughts/thoughts.js";
import { Projects, ProjectsPagesEnum } from "./components/projects/projects.js";
import Games from "./components/projects/games.js";
import Tabletop from "./components/projects/tabletop.js";

const TopLayout = {
  view: (vnode) => {
    return m("main", [
      m(Header, { page: vnode.attrs.page }),
      m(".sl-content.pure-g", vnode.children),
    ]);
  },
};

const TopBestOfLayout = {
  view: (vnode) => {
    return m(
      TopLayout,
      { page: PagesEnum.bestof },
      m(BestOfLayout, { page: vnode.attrs.page }, vnode.children)
    );
  },
};

const TopThoughtsLayout = {
  view: (vnode) => {
    return m(
      TopLayout,
      { page: PagesEnum.thoughts },
      m(ThoughtsLayout, { thought: vnode.attrs.thought })
    );
  },
};

const TopProjectsLayout = {
  view: (vnode) => {
    let c = Games;
    if (vnode.attrs.project.name === "Tabletop") {
      c = Tabletop;
    }
    return m(
      TopLayout,
      { page: PagesEnum.projects },
      m(c, { project: vnode.attrs.project })
    );
  },
};

const TopLayoutResolver = (pagesEnumValue, children) => {
  return {
    render: () => {
      return m(TopLayout, { page: pagesEnumValue }, children);
    },
  };
};

const TopBestOfLayoutResolver = {
  render: (vnode) => {
    const bestOfPage = BestOfPagesEnum[vnode.attrs.listname];
    if (!bestOfPage) {
      return m.route.SKIP;
    }
    return m(TopBestOfLayout, { page: bestOfPage });
  },
};

const TopThoughtsLayoutResolver = {
  render: (vnode) => {
    const thought = ThoughtsMetadata[vnode.attrs.thought];
    if (!thought) {
      return m.route.SKIP;
    }
    return m(TopThoughtsLayout, { thought: thought });
  },
};

const TopProjectsResolver = {
  render: (vnode) => {
    const project = ProjectsPagesEnum[vnode.attrs.project];
    if (!project || (project.name !== "Games" && project.name !== "Tabletop")) {
      return m.route.SKIP;
    }
    return m(TopProjectsLayout, { project: project });
  },
};

const Routes = {};
Routes[PagesEnum.home.routeName] = TopLayoutResolver(PagesEnum.home, m(Home));
Routes[PagesEnum.now.routeName] = TopLayoutResolver(PagesEnum.now, m(Now));
Routes[PagesEnum.books.routeName] = TopLayoutResolver(
  PagesEnum.books,
  m(GoodReads)
);
Routes[PagesEnum.bestof.routeName] = TopLayoutResolver(
  PagesEnum.bestof,
  m(BestOf)
);
Routes[`${PagesEnum.bestof.routeName}/:listname`] = TopBestOfLayoutResolver;
Routes[PagesEnum.projects.routeName] = TopLayoutResolver(
  PagesEnum.projects,
  m(Projects)
);
Routes[`${PagesEnum.projects.routeName}/:project`] = TopProjectsResolver;
Routes[PagesEnum.thoughts.routeName] = TopLayoutResolver(
  PagesEnum.thoughts,
  m(Thoughts)
);
Routes[`${PagesEnum.thoughts.routeName}/:thought`] = TopThoughtsLayoutResolver;
Routes[PagesEnum.credits.routeName] = TopLayoutResolver(
  PagesEnum.credits,
  m(Credits)
);

m.route(document.body, "/", Routes);
