import m from "mithril";

// Note: This is a poor man's markdown. If you want
// the real thing, look elsewhere.

/* -----------------------------------------------
 *
 * Helpers
 *
 * -----------------------------------------------
 */

class HandlerResult {
  constructor(newIdx, outputs) {
    this.newIdx = newIdx;
    this.outputs = outputs.concat([""]);
  }
}

const LINK_BEGIN = "[";
const LINK_MIDDLE = "](";
const LINK_END = ")";

const linkHandler = (p, i) => {
  const linkEnd = p.indexOf(LINK_END, i);
  const linkMiddle = p.indexOf(LINK_MIDDLE, i);
  const link = p.substring(linkMiddle + 2, linkEnd);
  const label = p.substring(i + 1, linkMiddle);

  let tag = "a";
  if (link.startsWith("/")) {
    tag = m.route.Link;
  }

  return new HandlerResult(linkEnd, [
    m(tag, { href: link, target: "_blank" }, label),
  ]);
};

const CODE_START_END = "`";
const CODE_NOWRAP_LIMIT = 32;

const codeHandler = (p, i) => {
  const codeEnd = p.indexOf(CODE_START_END, i + 1);
  const code = p.substring(i + 1, codeEnd);

  let outputClass = "inline";
  if (code.length < CODE_NOWRAP_LIMIT) {
    outputClass += " code-nowrap";
  }

  return new HandlerResult(codeEnd, [m("code", { class: outputClass }, code)]);
};

const ITALIC_START_END = "*";
const BOLD_START_END = "**";
const BI_START_END = ITALIC_START_END + BOLD_START_END;

const boldItalicHandler = (p, i) => {
  let marker = ITALIC_START_END;
  let tags = ["em"];
  if (p.substring(i, i + BI_START_END.length) === BI_START_END) {
    marker = BI_START_END;
    tags.push("strong");
  } else if (p.substring(i, i + BOLD_START_END.length) === BOLD_START_END) {
    marker = BOLD_START_END;
    tags = ["strong"];
  }

  const end = p.indexOf(marker, i + marker.length);
  const text = p.substring(i + marker.length, end);

  return new HandlerResult(end + marker.length - 1, [
    tags.reduce((acc, t) => {
      return m(t, acc);
    }, text),
  ]);
};

/*
 * output : ["hello this is a normal output. ", "with multiple sentences. "]
 * output + ["Hello this is ", m("a", {href: "www.google.com"}, "link"), " in output."]
 */
const DEFAULT_HANDLERS = {
  [LINK_BEGIN]: linkHandler,
  [CODE_START_END]: codeHandler,
  [ITALIC_START_END]: boldItalicHandler,
};

const handleMarkdown = (p) => {
  let outputIdx = 0;
  let output = [""];
  for (let i = 0; i < p.length; i++) {
    const c = p[i];
    const match = Object.keys(DEFAULT_HANDLERS).filter((handler) => {
      return c === handler;
    });
    if (match.length > 0) {
      const res = DEFAULT_HANDLERS[match[0]](p, i);
      if (!res) {
        output[outputIdx] += c;
        continue;
      }

      i = res.newIdx;
      res.outputs.forEach((o) => {
        outputIdx += 1;
        output[outputIdx] = o;
      });
    } else {
      output[outputIdx] += c;
    }
  }
  return output;
};

export default handleMarkdown;
