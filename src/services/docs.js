import m from "mithril";

const onRejected = (fn) => {
  return () => {
    console.warn("Could not access resource");
    fn("");
  };
};

const DocService = {
  getThought: (thoughtId, fn) => {
    return m
      .request({
        method: "GET",
        url: `/static/thoughts/thought${thoughtId}.txt`,
        responseType: "text",
      })
      .then(fn, onRejected(fn));
  },
  getList: (listName, fn) => {
    return m
      .request({
        method: "GET",
        url: `/static/bestof/${listName.replace(/ /g, "")}.txt`,
        responseType: "text",
      })
      .then(fn, onRejected(fn));
  },
  splitIntoSections: (doc) => {
    return doc.split(/\r\n\r\n|\n\n/g);
  },
  splitIntoLines: (doc) => {
    return doc.split(/\r\n|\n/g);
  },
  timeToRead: (doc) => {
    const AVG_READ_SPEED = 260; // Per Minute
    const readMin = (doc.split(" ").length / AVG_READ_SPEED).toFixed(1);
    return `~${readMin} minute read`;
  },
};

export default DocService;
