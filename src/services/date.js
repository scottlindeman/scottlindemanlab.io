const TimeZone = "T12:00:00.000Z";

const DateService = {
  now: () => {
    return new Date();
  },

  createDate: (dStr) => {
    return new Date(dStr + TimeZone);
  },

  dateToDate: (d, includeYear = false) => {
    const day = d.toLocaleString("en-us", { weekday: "long" });
    const month = d.toLocaleString("en-us", { month: "long" });
    const dayOfMonth = d.getDate();

    if (includeYear) {
      return `${day}, ${month} ${dayOfMonth}, ${d.getFullYear()}`;
    } else {
      return `${day}, ${month} ${dayOfMonth}`;
    }
  },

  dateToTime: (d) => {
    const hours = d.getHours().toString().padStart(2, 0);
    const minutes = d.getMinutes().toString().padStart(2, 0);
    const seconds = (d.getSeconds() + 1).toString().padStart(2, 0);
    return `${hours}:${minutes}:${seconds}`;
  },

  timeOfDay: (d) => {
    const hours = d.getHours();

    if (hours >= 6 && hours < 12) {
      return "Good Morning.";
    } else if (hours >= 12 && hours < 17) {
      return "Good Afternoon.";
    } else if (hours >= 17 || hours < 2) {
      return "Good Evening.";
    } else {
      return "You Woke Me Up.";
    }
  },

  percentageOfDay: (d) => {
    const hours = d.getHours();
    const minutes = d.getMinutes() / 60;

    return (hours + minutes) / 24;
  },

  compareDates: (descending, isObj = false) => {
    const direction = descending ? -1 : 1;

    const cmp = (a, b) => {
      if (!!isObj) {
        a = a.date;
        b = b.date;
      }

      const d1 = DateService.createDate(a);
      const d2 = DateService.createDate(b);
      const result = d1.getTime() - d2.getTime();
      return result * direction;
    };

    return cmp;
  },
};

export default DateService;
