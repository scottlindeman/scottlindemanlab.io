const NavService = {
  createAnchor: (name) => {
    const nonalphar = /[\W]+/g;
    return name.toLowerCase().replaceAll(" ", "_").replace(nonalphar, "");
  },

  scrollToAnchor: (anchorName) => {
    const targetElem = document.querySelector(`h2[name="${anchorName}"]`);
    if (!targetElem) {
      return;
    }
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    const targetYValue = targetElem.getBoundingClientRect().top;
    window.scroll({
      top: targetYValue + scrollTop - 70, // The header height
      left: 0,
      behavior: "smooth",
    });
  },
};

export default NavService;
